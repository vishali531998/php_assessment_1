<?php 
    
    session_start();
    
    $error_array=array();
    if(isset($_POST['send'])){

        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "assessment";

        // Create connection
        $conn = mysqli_connect($servername, $username, $password,$dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " .$conn->connect_error);
        }

        $sql = "SELECT * FROM assessment_form where email = '".$_POST['email']."' or user_name ='".$_POST['username']."'";
        //echo $sql;exit;
        $sql1 = $conn->query($sql);  
        $row = $sql1->fetch_assoc();


        if (!($sql1->num_rows > 0))
        {
       
            $msg=array();
            $User_name = $_POST['username'];
            $User_password = $_POST['password'];
            $cnfrmpassword = $_POST['confirmpwd'];
            $User_age = $_POST['Age'];
            $User_mail = $_POST['email'];
            $User_phone = $_POST['Phone'];
            $User_zipcode = $_POST['ZipCode'];
            $User_money = $_POST['Money'];
            $User_dob = $_POST['DOB'];
            $User_website = $_POST['Website'];
            $User_comment = $_POST['Comment'];
            $value = 0;

            if(!preg_match('/^\w{5,50}$/', $User_name))
            { 
                $msg['username'] =  'This is not a valid username';        
                $value = 1;
            }
        
            if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,12}$/', $User_password))
            {
                $msg['pwd']='The password does not meet the requirements!';
                $value = 1;
            }

            if($_POST['password']!= $_POST['confirmpwd'])
            {
                $msg['mismatch']='Oops! Password did not match! Try again!';
                $value = 1;
            }

            if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $User_mail))
            {
        
                $msg['emailid'] =  'This is not a valid email';        
                $value = 1;
            }
        
            if(!preg_match('/^[0-9]{10}+$/', $User_phone))
            {
                $msg['Phonenob']='Not a valid mobile number!';
                $value = 1;
            }
        
            if(!preg_match('/^\d{6}$/', $User_zipcode))
            {
                $msg['zipcode']='Please enter valid ZipCode';
                $value = 1;
            } 

            if(!preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$User_website))
            {
                $msg['url'] =  'This is not a valid website';        
                $value = 1;
            }

            if($value == 0){
                
                $sql2 = "INSERT INTO assessment_form (user_name,password, cnfrm_password,age,email,phone,option_select,zipcode,money,dob,website,comment)
                VALUES ('".$User_name."','".$User_password."','".$cnfrmpassword."','".$User_age."','".$User_mail."','".$User_phone."','".$_POST['Option_select']."','".$User_zipcode."','".$User_money."','".$User_dob."','".$User_website."','".$User_comment."')";
                echo $sql2;
                //die;
                $result = $conn->query($sql2);
                $conn->close();

                if($result)
                {
                    print_r($_SESSION);
                    header("location:2.php");
                }
            }
            else{
                $_SESSION['error_msgs'] = $msg;
            }
        }
        else
        {
            $error_array['username'] = "Username already exists!!!";
            $error_array['email'] = "Email already exists!!!";
            $_SESSION['error_msgs'] = $error_array;
        }
    }    
?>        

    <?php
    
    if(isset($_SESSION['error_msgs']) && !empty($_SESSION['error_msgs'])){
        $err_array = $_SESSION['error_msgs'];
    }
    session_destroy();
    ?>

<!DOCTYPE html>
<html>
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">


		<!-- Website CSS style -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Website Font style -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		<link rel="stylesheet" href="1.css">
		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    </head>
	<body>
		<div class="container">
			<div class="row main">
				<div class="main-login main-center">
	
					<form class="" enctype="multipart/form-data" method="post" action="">
						                       
                        <div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Username</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="username" id="username"  placeholder="Enter your Username"/>
								</div>
                            </div>
                            <span> 
                                <?php 
                                    if(isset($err_array['username']) && !empty($err_array['username'])){
                                    echo '<span style= "color:black"> '.$err_array["username"].'</span>';
                                    }
                                ?>
                            </span>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
								</div>
                            </div>
                            <span> 
                                <?php 
                                    if(isset($err_array['pwd']) && !empty($err_array['pwd'])){
                                    echo '<span style= "color:black"> '.$err_array["pwd"].'</span>';
                                    }
                                ?>
                            </span>
						</div>

						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="confirmpwd" id="confirmpwd"  placeholder="Confirm your Password"/>
                                </div>
                            </div>
                            <span> 
                                <?php 
                                    if(isset($err_array['mismatch']) && !empty($err_array['mismatch'])){
                                    echo '<span style= "color:black"> '.$err_array["mismatch"].'</span>';
                                    }
                                ?>
                            </span>
						</div>

	                    <div class="form-group">
							<label for="Age" class="cols-sm-2 control-label">Age</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-heart" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="Age" id="Age"  placeholder="Enter your Age" readonly/>
								</div>
							</div>
                        </div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="email" id="email"  placeholder="Enter your Email"/>
								</div>
                            </div>
                            <span> 
                                <?php 
                                    if(isset($err_array['emailid']) && !empty($err_array['emailid'])){
                                    echo '<span style= "color:black"> '.$err_array["emailid"].'</span>';
                                    }
                                ?>
                            </span>
						</div>

		                <div class="form-group">
							<label for="Phone" class="cols-sm-2 control-label">Phone</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="Phone" id="Phone"  placeholder="### ### ####"/>
								</div>
                            </div>
                            <span> 
                                <?php 
                                    if(isset($err_array['Phonenob']) && !empty($err_array['Phonenob'])){
                                    echo '<span style= "color:black"> '.$err_array["Phonenob"].'</span>';
                                    }
                                ?>
                            </span>
                        </div>
                        
                        <div class="form-group">
							<label for="Option" class="cols-sm-2 control-label">Option</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></span>
                                    <select name="Option_select" id="Option" class="form-control">
                                        <option value="" selected disabled>----Please select your option----</option>
                                        <option>Engineer</option>
                                        <option>Doctor</option>
                                        <option>Police</option>
                                        <option>Lawyer</option>
                                        <option>Others</option>
                                    </select>
								</div>
							</div>
                        </div>
                        
                        <div class="form-group">
							<label for="ZipCode" class="cols-sm-2 control-label">Zip Code</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-tasks" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="ZipCode" id="ZipCode"  placeholder="Enter your ZipCode Number"/>
                                </div>
                            </div>
                            <span> 
                                <?php 
                                    if(isset($err_array['zipcode']) && !empty($err_array['zipcode'])){
                                    echo '<span style= "color:black"> '.$err_array["zipcode"].'</span>';
                                    }
                                ?>
                            </span>
                        </div>

                        <div class="form-group">
							<label for="Money" class="cols-sm-2 control-label">Money</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-usd" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="Money" id="Money"  placeholder="Enter Amount"/>
								</div>
							</div>
                        </div>

                        <div class="form-group">
							<label for="DOB" class="cols-sm-2 control-label">DOB</label>
							<div class="cols-sm-10">
								<div class="input-group">
                                    <span class="input-group-addon"><i span class="glyphicon glyphicon-calendar" aria-hidden="true"></i></span>
                                    <input type="date" class="form-control" name="DOB" id="DOB"  placeholder="(YYYY-MM-DD)">
								</div>
							</div>
                        </div>
                        
                        <div class="form-group">
							<label for="Website" class="cols-sm-2 control-label">Website</label>
							<div class="cols-sm-10">
							<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-link" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="Website" id="Website"  placeholder="Enter Website"/>
                                </div>
                            </div>
                            <span> 
                                <?php 
                                    if(isset($err_array['url']) && !empty($err_array['url'])){
                                    echo '<span style= "color:black"> '.$err_array["url"].'</span>';
                                    }
                                ?>
                            </span>
                        </div>

                        <div class="form-group">
							<label for="Comment" class="cols-sm-2 control-label">Comment</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></span>
									<textarea class="form-control" name="Comment" id="Comment"  placeholder="Enter Textarea"></textarea>
								</div>
							</div>
                        </div>

						<div class="form-group ">
                            <button type="send" name="send" class="btn btn-info">
                            <span class="glyphicon glyphicon-send"></span>Send
                            </button>
						</div>
						
					</form>
				</div>
			</div>
		</div>
    </body>
</html>