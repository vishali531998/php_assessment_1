<?php

    session_start();

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "assessment";

    // Create connection
    $conn = mysqli_connect($servername, $username, $password,$dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM assessment_form";
    $result = $conn->query($sql);
    //echo $result->num_rows ;die;
    
    if ($result->num_rows > 0) 
    {

        $html='
            <table style="width:100%">
                <tr>
                    <td><b>S.No</b></td>
                    <td><b>UserName</b></td>
                    <td><b>Email</b></td> 
                    <td><b>Phone</b></td>
                    <td><b>Age</b></td>
                    <td><b></b></td>
                </tr>  
                
        ';

        while($row = $result->fetch_assoc()){

            $html.='<tr> 
            <td>'.$row["id"].'</td>
            <td>'.$row["user_name"].'</td>
            <td>'.$row["email"].'</td>
            <td>'.$row["phone"].'</td>
            <td>'.$row["age"].'</td>
            <td><a href="3.php? id='.$row["id"].'">edit</a></td>
            <td><a href="4.php? id='.$row["id"].'">delete</a></td>
            ';
        }

        $html.='</tr>';

        echo $html;
    } 
    else 
    {
        echo "0 results";
    }
    $conn->close();

?>