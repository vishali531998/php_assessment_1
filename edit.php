<?php 

    if(isset($_GET['id']) && !empty($_GET['id'])){

        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "assessment";

        // Create connection
        $conn = mysqli_connect($servername, $username, $password,$dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " .$conn->connect_error);
        }

        $id = $_GET['id'];

        $sql = "SELECT * FROM assessment_form where id = '".$id."'";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();

        session_start();
            
        if(isset($_POST['Submit'])){

            $sql = "SELECT * FROM assessment_form where email = '".$_POST['email']."' or user_name ='".$_POST['username']."'";
            
            $sql1 = $conn->query($sql);        

            if (!($sql1->num_rows > 0)) {
                
                $error_array=array();
                $user_name = $_POST['username'];
                $email = $_POST['email'];
                $phone = $_POST['Phone'];
                $age = $_POST['Age'];
                $error = 0;

                $sql = "SELECT * FROM assessment_form where id = '".$_GET['id']."'";
                $result = $conn->query($sql);
                $conn->close();

                if( $error = 0){

                    $sql = "UPDATE assessment_form
                        SET
                    user_name = '".$_POST['username']."',
                    email = '".$_POST['email']."', 
                    phone = '".$_POST['Phone']."', 
                    age = '".$_POST['Age']."', 
                    where id = '".$id."'
                    ";
                    $result = $conn->query($sql);

                    if($result){
                        print_r($_SESSION);
                        header("location:2.php");
                    }
                }
                else{
                    $_SESSION['error_msgs'] = $error_array;
                }
            }
            else{
                $error_array['username'] = "Username already exists!!!";
                $error_array['email'] = "Email already exists!!!";
                $_SESSION['error_msgs'] = $error_array;
            }
        }
        if(isset($_SESSION['error_msgs']) && !empty($_SESSION['error_msgs'])){
            $err_array = $_SESSION['error_msgs'];
            
        }   
    }
?>

<!DOCTYPE html>
<html>
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">


		<!-- Website CSS style -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Website Font style -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		<link rel="stylesheet" href="1.css">
		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    </head>
	<body>
    <div class="container">
        <div class="row main">
            <div class="main-login main-center">

                <form class="" enctype="multipart/form-data" method="post" action="">
                                            
                    <div class="form-group">
                        <label for="username" class="cols-sm-2 control-label">Username</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="username" id="username"  placeholder="Enter your Username"
                                value="<?php if(isset($row["user_name"]))
                                {
                                    echo $row["user_name"];
                                }
                                else
                                {
                                    echo '';
                                };
                                ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="cols-sm-2 control-label">Email</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="email" id="email"  placeholder="Enter your Email"
                                value="<?php if(isset($row["email"]))
                                {
                                    echo $row["email"];
                                }
                                else
                                {
                                    echo '';
                                };
                                ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Phone" class="cols-sm-2 control-label">Phone</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="Phone" id="Phone"  placeholder="### ### ####"
                                value="<?php if(isset($row["phone"]))
                                {
                                    echo $row["phone"];
                                }
                                else
                                {
                                    echo '';
                                };
                                ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Age" class="cols-sm-2 control-label">Age</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-heart" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="Age" id="Age" placeholder="Enter your Age" readonly
                                value="<?php if(isset($row["age"]))
                                {
                                    echo $row["age"];
                                }
                                else
                                {
                                    echo '';
                                };
                                ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group ">
                        <button type="Submit" name="Submit" class="btn btn-info">Submit
                        <a href="2.php"></a>
                        </button>
                    </div>

                </form>

            </div>

        </div>

    </div>

</html>